<?php

use yii\db\Migration;

/**
 * Handles the creation for table `event`.
 */
class m160907_080017_create_event_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('event', [
            'eventId' => 'pk',
            'title' => 'string',
            'description' => 'string',
            'created_at' => 'integer',
            'updated' => 'integer',
            'date' => 'date',
            'created_by' => 'integer',
            'updated_by' => 'integer',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('event');
    }
}
