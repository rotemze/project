<?php

use yii\db\Migration;

/**
 * Handles the creation for table `activity`.
 */
class m160907_084622_create_activity_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('activity', [
            'activityNumber' => 'pk',
            'date' => 'date',
            'subject' => 'text',
            'content' => 'text',
            'difficult' => 'text',
            'goodMoments' => 'text',
            'lessonsLearned' => 'text',
            'generalImprission' => 'text',
            'expectations' => 'text',
            'comments' => 'text',
            'pupil' => 'string',
            'created_at' => 'integer',
            'updated_at' => 'integer',
            'created_by' => 'integer',
            'updated_by' => 'integer',

        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('activity');
    }
}
