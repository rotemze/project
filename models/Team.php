<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;    
use yii\db\ActiveRecord;
use app\models\User;
use app\models\Pupil;

/**
 * This is the model class for table "team".
 *
 * @property integer $teamNumber
 * @property string $teamName
 * @property string $groupName
 * @property string $dayActive
 * @property integer $hourActive
 * @property integer $id
 */
    class Team extends \yii\db\ActiveRecord 
{
    public $coord; 
    public $guide;
    public $pupil;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'team';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        //    [['hourActive'], 'integer'],
           [['id','guideId1','teamNumber'], 'integer'],
            [['teamName', 'groupName','hourActive'], 'string', 'max' => 255],
            ['dayActive', 'required'],
            ['teamName', 'required'],

            //['coord', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'teamNumber' => 'Team Number',
            'teamName' => 'Team Name',
            'groupName' => 'Group Name',
            'dayActive' => 'Day Active',
            'hourActive' => 'Hour Active',
            'id' => 'Coordinator',
            'guideId1' => 'guide',
          //  'coord' => 'coordinators',
        ];

    }


   public static function getTeams()
    {
        $allTeams = self::find()->all();
        $allTeamsArray = ArrayHelper::
                    map($allTeams, 'teamNumber', 'teamName');
        return $allTeamsArray;                       
    }
   public static function getcoord()
    {  
    $coord = Yii::$app->db->createCommand(   "SELECT u.username , u.id
      FROM user u 
INNER JOIN auth_assignment a
        ON u.id = a.user_id
INNER JOIN auth_item ai
        ON ai.name = a.item_name
     WHERE a.item_name = 'coordinator'" )->queryAll();  
       $coord = ArrayHelper::
       map($coord, 'id', 'username');
$coord[-1] = 'All coordinators';
        $coord = array_reverse ( $coord, true );
        return $coord;  
    }

 
         public static function getguide()
    {
    $guide = Yii::$app->db->createCommand(   "SELECT u.username , u.id , u.firstname , u.lastname
      FROM user u 
INNER JOIN auth_assignment a
        ON u.id = a.user_id
INNER JOIN auth_item ai
        ON ai.name = a.item_name
     WHERE a.item_name = 'guide'" )->queryAll();  
       $guide = ArrayHelper::
       map($guide, 'id', 'username');
$guide[-1] = 'All guides';
        $guide = array_reverse ( $guide, true );
        return $guide;  
    }

   
    
    public function getName(){

        return $this->teamName;
    }
      public function getTeamNumber(){

        return $this->teamNumber;
    }
       public function getG($guide){

        return $this->name;
    }
    public function getUserGuide()
    {
        return $this->hasOne(User::className(), ['id' => 'guideId1']);
    }
     public function getUserCoord()
    {
        return $this->hasOne(User::className(), ['id' => 'id']);
    }
     public function getPupilTeam() //give many team
    {
         return $this->hasMany(Pupil::className(), ['teamNumber' => 'teamNumber']);
    }
    // public function extraFields()
    //     {
    //         return [
    //             'teamNumber'=>function() {
    //                 if (isset($this->teamNumber)) {
    //                     $teamNumber = [];
    //                     foreach($this->teamNumber as $teamNumber) {
    //                         $teamNumber[] = $teamNumber->teamNumber;
    //                     }
    //                     return implode(', ', $teamNumber);
    //                 }
    //             }
    //         ];
    //     }
   }
