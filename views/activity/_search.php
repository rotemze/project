<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ActivitySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="activity-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'activityNumber') ?>

    <?= $form->field($model, 'date') ?>

    <?= $form->field($model, 'subject') ?>

    <?= $form->field($model, 'content') ?>

    <?= $form->field($model, 'difficult') ?>

    <?php // echo $form->field($model, 'goodMoments') ?>

    <?php // echo $form->field($model, 'lessonsLearned') ?>

    <?php // echo $form->field($model, 'generalImprission') ?>

    <?php // echo $form->field($model, 'expectations') ?>

    <?php // echo $form->field($model, 'comments') ?>

    <?php // echo $form->field($model, 'pupil') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
