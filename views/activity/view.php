<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\pupil;

/* @var $this yii\web\View */
/* @var $model app\models\Activity */

$this->title = $model->activityNumber;
$this->params['breadcrumbs'][] = ['label' => 'Activities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activity-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->activityNumber], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->activityNumber], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'activityNumber',
            'date',
            'subject:ntext',
            'content:ntext',
            'difficult:ntext',
            'goodMoments:ntext',
            'lessonsLearned:ntext',
            'generalImprission:ntext',
            'expectations:ntext',
            'comments:ntext',
          //  'pupil',
                 [ 
                'label' => $model->attributeLabels()['pupil'],
                'format' => 'html',
                 'value' => $model->pupilsnames,
            
            ], 
        ],
    ]) ?>

</div>
