<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\user;

//use app\assets\AdminAsset;


AppAsset::register($this);
//AdminAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
     //   'brandLabel' => '<img src = "images/sky.png ">',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
          // !Yii::$app->user->isGuest ?
          //   ['label' => 'Home', 'url' => ['/site/index']]
          //   :
          //   '',
              \Yii::$app->user->can('updateUser') && 
        !Yii::$app->user->isGuest ?  
            ['label'=>'users', 'url'=>['user/index']]

            :
            '',
           \Yii::$app->user->can('indexTeams') && 
        !Yii::$app->user->isGuest ?  
          ['label'=>'teams', 'url'=>['team/index']]
            :
            '',
                   \Yii::$app->user->can('indexPupil') && 
        !Yii::$app->user->isGuest ? 
            ['label'=>'pupils', 'url'=>['pupil/index']]
            :
            '',
                   \Yii::$app->user->can('indexActivity') && 
        !Yii::$app->user->isGuest ? 
            ['label'=>'activities', 'url'=>['activity/index']]
             :
            '',
                   \Yii::$app->user->can('indexActivity') && 
        !Yii::$app->user->isGuest ? 
            ['label'=>'events', 'url'=>['event/index']]
             :
            '',
         // !Yii::$app->user->isGuest ?
            // ['label'=>'reports', 'url'=>['reports/index']],
            //  :
            // '',
                      \Yii::$app->user->can('indexActivity') && 
        !Yii::$app->user->isGuest ? 
            ['label' => 'sticky notes', 'url' => ['/note']]
             :
            '',
         !Yii::$app->user->isGuest ?
              ['label' => 'about', 'url' => ['/site/about']]
              :
            '', 
            //   !Yii::$app->user->isGuest ?
            //     ['label' => 'Login', 'url' => ['/site/login']]
            //   :
            // '', 
                      Yii::$app->user->isGuest ? (
                        ''

                ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post', ['class' => 'navbar-form'])
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username .  ')',
                    ['class' => 'btn btn-link']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">

        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>
<!-- 
<footer class="footer">
    <div class="container">
       <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p> 
    </div>
</footer>
-->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>