<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Team;
use app\models\Pupil;

/* @var $this yii\web\View */
/* @var $model app\models\Pupil */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pupil-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phoneNumber')->textInput() ?>

    <?= $form->field($model, 'fatherName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fatherPhone')->textInput() ?>

    <?= $form->field($model, 'motherName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'motherPhone')->textInput() ?>

    <?= $form->field($model, 'homePhone')->textInput() ?>

    <?= $form->field($model, 'initialQues')->radioList(array('yes'=>'Yes','no'=>'No')); ?>

    <?= $form->field($model, 'forms')->radioList(array('yes'=>'Yes','no'=>'No')); ?>

    

<?= $form->field($model, 'teamNumber')->
                dropDownList(team::getTeams()) ?>      
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
