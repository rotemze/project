<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PupilSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pupil-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'pupilId') ?>

    <?= $form->field($model, 'firstname') ?>

    <?= $form->field($model, 'lastname') ?>

    <?= $form->field($model, 'phoneNumber') ?>

    <?= $form->field($model, 'fatherName') ?>

    <?php // echo $form->field($model, 'fatherPhone') ?>

    <?php // echo $form->field($model, 'motherName') ?>

    <?php // echo $form->field($model, 'motherPhone') ?>

    <?php // echo $form->field($model, 'homePhone') ?>

    <?php // echo $form->field($model, 'initialQues') ?>

    <?php // echo $form->field($model, 'forms') ?>

    <?php // echo $form->field($model, 'teamNumber') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
