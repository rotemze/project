<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\team;
use app\models\pupil;

/* @var $this yii\web\View */
/* @var $model app\models\Pupil */

$this->title = $model->pupilname;
$this->params['breadcrumbs'][] = ['label' => 'Pupils', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pupil-view">

    <h1><?= Html::encode($this->title) ?></h1>
<?php if (\Yii::$app->user->can('createUser')) { ?>     

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->pupilId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->pupilId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
  <?php } ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'pupilId',
            'firstname',
            'lastname',
            'phoneNumber',
            'fatherName',
            'fatherPhone',
            'motherName',
            'motherPhone',
            'homePhone',
            'initialQues',
            'forms',
           //'teamNumber',
            [ 
                'label' => $model->attributeLabels()['teamNumber'],
                'format' => 'html',
                'value' => isset($model->teamNum->name) ? $model->teamNum->name : 'No one!',  
					//['team/view', 'teamNumber' => $model->teamNum->teamNumber],
            ], 
        ],
    ]) ?>

</div>
