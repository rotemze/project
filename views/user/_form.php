<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;


/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

    
<!--
    <?= $form->field($model, 'auth_key')->textInput(['maxlength' => true]) ?>-->

    <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'role')->dropDownList($roles) ?>       


   <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'institute')->textInput(['maxlength' => true]) ?>


<?= $form->field($model, 'birthday')->widget(\yii\jui\DatePicker::classname(), [
    'language' => 'en',
    'dateFormat' => 'yyyy-MM-dd',
]) ?>

    <?= $form->field($model, 'phoneNumber')->textInput() ?>

   
     <?= $form->field($model, 'yearGrade')->dropDownList(array('1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5')) ?>

    
    <?= $form->field($model, 'sizeShirt')->radioList(array('xs'=>'XS','s'=>'S','m'=>'M','xl'=>'XL','xxl'=>'XXL')); ?>

    <?= $form->field($model, 'fieldStudy')->textInput(['maxlength' => true]) ?>
<!-- 
    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?> -->

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
